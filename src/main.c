#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

void debug(const char *fmt, ...);
struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
    void* allocated = mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
    if (allocated== MAP_FAILED) allocated = NULL;
    return allocated;
}


void check_memory_allocation() {
    debug("Starting memory allocation test.\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap != NULL);
    
    void* first_allocation = _malloc(1024);
    assert(first_allocation != NULL);

    void* second_allocation = _malloc(2048);
    assert(second_allocation != NULL);

    heap_term();
    debug("Memory allocation test successful.\n");
}

void test_one() {
    debug("Initiating double block free test from triple allocation\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap);

    void* block_1 = _malloc(1024);
    void* block_2 = _malloc(2048);
    void* block_3 = _malloc(4096);
    assert(block_1 && block_2 && block_3);

    assert(!block_get_header(block_1)->is_free);
    assert(!block_get_header(block_2)->is_free);
    assert(!block_get_header(block_3)->is_free);

    _free(block_1);
    assert(block_get_header(block_1)->is_free);
    assert(!block_get_header(block_2)->is_free);
    assert(!block_get_header(block_3)->is_free);
    heap_term();

    debug("Double block free test concluded successfully.\n");
}

void test_two(){
    debug("test free two block of three\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    void* block_1 = _malloc(1024);
    void* block_2 = _malloc(2048);
    void* block_3 = _malloc(4096);
    assert(block_1 && block_2 && block_3);

    assert(!block_get_header(block_1)->is_free);
    assert(!block_get_header(block_2)->is_free);
    assert(!block_get_header(block_3)->is_free);

    _free(block_2);
    _free(block_3);

    assert(!block_get_header(block_1)->is_free);
    assert(!block_get_header(block_2)->is_free);
    assert(!block_get_header(block_3)->is_free);

    heap_term();
    debug("test passed\n");
}

void validate_heap_growth() {
    debug("Heap Growth Validation: Starting\n");
    struct region *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
    
    size_t heap_size = heap->size;
    _malloc(REGION_MIN_SIZE * 2);  
    size_t new_heap_size = heap->size;

    assert(new_heap_size > heap_size); 
    heap_term();
    debug("Heap Growth Validation: Successful\n");
}

void verify_region_allocation_separation() {
    debug("Verifying allocation of a new region at a distinct location\n");
    void *allocated = map_pages(HEAP_START, REGION_MIN_SIZE, MAP_FIXED);
    assert(allocated);
    void *subsequent_allocated = _malloc(REGION_MIN_SIZE);
    assert(subsequent_allocated);
    assert(allocated!= subsequent_allocated);
    debug("Verification of separate region allocation successful\n");
}

int main(){
    check_memory_allocation();
    test_one();
    test_two();
    validate_heap_growth();
    verify_region_allocation_separation();
}
